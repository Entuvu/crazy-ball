﻿using UnityEngine;

public class MaskController : MonoBehaviour
{
    public GameObject myPaddle;
    public PolygonCollider2D myPaddlePolgyon;

    private float closestDistance;
    private Vector2 closestPoint;

    [SerializeField] private Transform rotationBasedObject;

    [SerializeField] private GameObject pointObject;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Ball")
        {
            collision.GetComponent<MoveBall>().inZone = true;
            myPaddle.GetComponent<Collider2D>().isTrigger = false;
        }
    }

    private void Update()
    {
        Vector2 c;
        if (GameManager.Instance.ioDevice == GameManager.IODevice.KEYBAORD)
            c = rotationBasedObject.position;
        else
            c = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        closestPoint = myPaddlePolgyon.transform.TransformPoint(myPaddlePolgyon.points[0]);
        closestDistance = Vector2.Distance(closestPoint, c);
        
        for (int x = 0; x < myPaddlePolgyon.points.Length;  x++)
        {
            Vector2 a = myPaddlePolgyon.transform.TransformPoint(myPaddlePolgyon.points[x]);
            Vector2 b;
            if (x == myPaddlePolgyon.points.Length -1)
                b = myPaddlePolgyon.transform.TransformPoint(myPaddlePolgyon.points[0]);
            else
                b = myPaddlePolgyon.transform.TransformPoint(myPaddlePolgyon.points[x + 1]);

            Vector2 answer = Math3d.ProjectPointOnLineSegment(a, b, c);
            float distance = Vector2.Distance(answer, c);

            if(distance < closestDistance)
            {
                closestPoint = answer;
                closestDistance = distance;
            }
        }
        pointObject.transform.position = closestPoint;
        //Debug.Log(closestPoint);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Ball")
        {
            collision.GetComponent<MoveBall>().inZone = false;
            myPaddle.GetComponent<Collider2D>().isTrigger = true;

        }
    }

    //private Vector3 ClosestPoint(Vector3 vA, Vector3 vB , Vector3 vPoint )
    //{
    //    Vector3 a = Vector3.Project((vPoint - vA), (vB - vA)) + vA;

       
    //    return a;
   
    //}



    //private Vector2 ClosestPointOnLine(Vector2 vA, Vector2 vB, Vector2 vPoint )
    //{
    //    var vVector1 = vPoint - vA;
    //    var vVector2 = (vB - vA).normalized;

    //    var d = Vector2.Distance(vA, vB);
    //    var t = Vector2.Dot(vVector2, vVector1);

    //    if (t <= 0)
    //        return vA;

    //    if (t >= d)
    //        return vB;

    //    var vVector3 = vVector2 * t;

    //    var vClosestPoint = vA + vVector3;

    //    return vClosestPoint;
    //}
}
