﻿using UnityEngine;

public class KeyboardRotator : MonoBehaviour
{
    public float clampZ = 120;

    public PaddleController.PLAYERCOLOR playerColor;

    string axis = "";
    int inverse = 1;

    private void Start()
    {
        if (playerColor == PaddleController.PLAYERCOLOR.GREEN)
        {
            axis = "P1Vertical";
            inverse = -1;
        }
        else
        {
            axis = "P2Vertical";
        }
    }

    private void Update()
    {
        var a = -Input.GetAxis(axis);
            
        bool freezeUp = false;
        bool freezeDown = false;

        if (!freezeDown && !freezeUp)
        {
            transform.Rotate(Vector3.forward, 10 * a * inverse);
        }
        else if (!freezeDown && a < 0)
        {
            transform.Rotate(Vector3.forward, 10 * a * inverse);
        }
        else if (!freezeUp && a > 0)
        {
            transform.Rotate(Vector3.forward, 10 * a * inverse);
        }
    }
}
