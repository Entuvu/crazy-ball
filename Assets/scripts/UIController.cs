﻿using UnityEngine;
using UnityEngine.UI;

public class UIController: MonoBehaviour
{
    public static UIController Instance;

    [SerializeField] private Image blueHealthImage;
    [SerializeField] private Image greenHealthImage;
    [SerializeField] private float healthBarSpeed = 5f;
    [SerializeField] private TMPro.TextMeshProUGUI winner;
    [SerializeField] private TMPro.TextMeshProUGUI buttonText;

    private bool blueHealthUpdated = false;
    private bool greenHealthUpdated = false;
    
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Update()
    {
        if (blueHealthUpdated)
        {
            var a = GameManager.Instance.bluePlayer.health / GameManager.Instance.bluePlayer.maxHealth;
            blueHealthImage.fillAmount = Mathf.Lerp(blueHealthImage.fillAmount, a, healthBarSpeed * Time.deltaTime);
            if (Mathf.Abs(blueHealthImage.fillAmount - a) < 0.01f)
            {
                blueHealthUpdated = false;
            }
        }

        if (greenHealthUpdated)
        {
            var a = GameManager.Instance.greenPlayer.health / GameManager.Instance.greenPlayer.maxHealth;
            greenHealthImage.fillAmount = Mathf.Lerp(greenHealthImage.fillAmount, a, healthBarSpeed * Time.deltaTime);
            if (Mathf.Abs(greenHealthImage.fillAmount - a) < 0.01f)
            {
                greenHealthUpdated = false;
            }
        }
    }

    public void StartGame()
    {
        buttonText.gameObject.transform.parent.parent.gameObject.SetActive(false);
        GameManager.Instance.StartGame();
    }

    public void ShowWinner(PaddleController.PLAYERCOLOR losingColor)
    {
        switch (losingColor)
        {
            case PaddleController.PLAYERCOLOR.GREEN:
                winner.text = "Blue Wins";
                winner.color = GameManager.Instance.blueColor;
                break;
            case PaddleController.PLAYERCOLOR.BLUE:
                winner.text = "Green Wins";
                winner.color = GameManager.Instance.greenColor;
                break;
        }
        buttonText.transform.parent.parent.gameObject.SetActive(true);
        buttonText.text = "Start Over";
        winner.gameObject.SetActive(true);
        GameManager.Instance.StopGame();
    }

    public void UpdateHealth(PaddleController.PLAYERCOLOR wallColor)
    {
        if(wallColor == PaddleController.PLAYERCOLOR.GREEN)
        {
            greenHealthUpdated = true;
        }
        else
        {
            blueHealthUpdated = true;
        }
    }
}