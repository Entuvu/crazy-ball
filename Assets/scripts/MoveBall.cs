﻿using UnityEngine;

public class MoveBall : MonoBehaviour
{
    public static MoveBall Instance;

    [SerializeField][Range(0,10)] private float movementSpeed = 10f;
    [SerializeField] [Range(0, 10)] private float bendSpeed = 10f;

    private Rigidbody2D rb;
    private Vector3 intendedRotation;

    [SerializeField] private AudioClip bounce;
    [SerializeField] private AudioClip explode;

    public bool moving = false;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public bool inZone = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if(moving)
            rb.AddForce(transform.right * movementSpeed);
    }

    private void Update()
    {
        if(moving)
            transform.right = Vector3.Lerp(transform.right, intendedRotation, bendSpeed * Time.deltaTime);
    }

    internal void StartGame()
    {
        rb.velocity = Vector3.zero; // kill previous momentum
        rb.angularVelocity =  0f;

        rb.isKinematic = false;
        transform.position = new Vector3(0, 0, 0);
        var euler = transform.eulerAngles;
        euler.z = UnityEngine.Random.Range(0f, 360f);
        transform.eulerAngles = euler;

        intendedRotation = transform.right;
        SetColor(GameManager.Instance.defaultColor);
        moving = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Border")
        {
            Debug.Log("Border hit");
            BounceBorder(collision);
            GetComponent<AudioSource>().PlayOneShot(explode);
        }else if(collision.transform.tag == "Paddle")
        {
            GetComponent<AudioSource>().PlayOneShot(bounce);
            
            BouncePaddle(collision);
        }
    }

    internal void StopGame()
    {
        moving = false;
        transform.position = new Vector3(0, 0, 0);
        rb.isKinematic = true;
    }

    private void BouncePaddle(Collision2D collision)
    {

        var nml = collision.contacts[0].normal;
        var c = collision.transform.GetComponent<PaddleController>().playerColor;

        if (Vector2.Distance(nml,Vector2.left) < 0.2f || Vector2.Distance(nml, Vector2.right)< .2f)
        {
            intendedRotation = new Vector3(-transform.right.x, transform.right.y, transform.right.z);
        }

        if (Vector2.Distance(nml, Vector2.up) < 0.2f || Vector2.Distance(nml, Vector2.down) < .2f)
        {
            intendedRotation = new Vector3(transform.right.x, -transform.right.y, transform.right.z);
        }

        rb.AddForce(100 * GameManager.Instance.vollyCount * collision.contacts[0].normal);
        if (c == PaddleController.PLAYERCOLOR.BLUE)
            SetColor(GameManager.Instance.blueColor);
        else
            SetColor(GameManager.Instance.greenColor);
        GameManager.Instance.vollyCount++;
    }

    private void SetColor(Color color)
    {
        GetComponent<SpriteRenderer>().color = color;
    }

    private void BounceBorder(Collision2D collision)
    {
        Border b = collision.transform.GetComponent<Border>();
        var collisionPosition = b.position;
        var rot = transform.eulerAngles;
        if (collisionPosition == Border.POS.RIGHT)
        {
            intendedRotation = new Vector3(-transform.right.x, transform.right.y, transform.right.z);
        }
        else if (collisionPosition == Border.POS.LEFT)
        {
            intendedRotation = new Vector3(-transform.right.x, transform.right.y, transform.right.z);
        }
        else if (collisionPosition == Border.POS.TOP)
        {
            intendedRotation = new Vector3(transform.right.x, -transform.right.y, transform.right.z);
        }
        else if (collisionPosition == Border.POS.BOTTOM)
        {
            intendedRotation = new Vector3(transform.right.x, -transform.right.y, transform.right.z);
        }

        if (b.wallColor == PaddleController.PLAYERCOLOR.GREEN)
        {
            if (GetComponent<SpriteRenderer>().color == GameManager.Instance.blueColor || GetComponent<SpriteRenderer>().color == GameManager.Instance.defaultColor)
            {
                GameManager.Instance.TakeDamage(b.wallColor, 10f);
            }
        }
        else
        {
            if (GetComponent<SpriteRenderer>().color == GameManager.Instance.greenColor || GetComponent<SpriteRenderer>().color == GameManager.Instance.defaultColor)
            {
                GameManager.Instance.TakeDamage(b.wallColor, 10f);
            }
        }
    }
}
